package programa;

import java.util.Scanner;

import clases.GestorDiscos;

public class Programa {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		GestorDiscos tiendaDiscos = new GestorDiscos();
		int opcion;

		do {
			System.out.println("");
			System.out.println("Practica 6          __________________         Enrique Mu�oz");
			System.out.println("");
			System.out.println("____________________________________________________________");
			System.out.println("");
			System.out.println("                    Elige unna opci�n:               ");
			System.out.println();
			System.out.println("      ARTISTA");
			System.out.println("1.  - Dar de alta");
			System.out.println("2.  - Listar");
			System.out.println("3.  - Buscar");
			System.out.println("4.  - Eliminar");
			System.out.println();
			System.out.println("      DISCO CD");
			System.out.println("5.  - Dar de alta");
			System.out.println("6.  - Listar");
			System.out.println("7.  - Buscar");
			System.out.println("8.  - Eliminar");
			System.out.println("9.  - Asignar artista");
			System.out.println();
			System.out.println("      DISCO VINILO");
			System.out.println("10. - Dar de alta");
			System.out.println("11. - Listar");
			System.out.println("12. - Buscar");
			System.out.println("13. - Eliminar");
			System.out.println("14. - Asignar artista");
			System.out.println();
			System.out.println("15. - Salir");
			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				tiendaDiscos.altaArtista();
				break;
			case 2:
				tiendaDiscos.listarArtistas();
				break;
			case 3:
				System.out.println(tiendaDiscos.buscarArtista());
				break;
			case 4:
				tiendaDiscos.eliminarArtista();
				break;
			case 5:
				tiendaDiscos.altaDiscoCd();
				break;
			case 6:
				tiendaDiscos.listarDiscosCd();
				break;
			case 7:
				System.out.println(tiendaDiscos.buscarDiscoCd());
				break;
			case 8:
				tiendaDiscos.eliminarDiscoCd();
				break;
			case 9:
				tiendaDiscos.asignarArtistaCd();
				break;
			case 10:
				tiendaDiscos.altaDiscoVinilo();
				break;
			case 11:
				tiendaDiscos.listarDiscosVinilo();
				break;
			case 12:
				System.out.println(tiendaDiscos.buscarDiscoVinilo());
				break;
			case 13:
				tiendaDiscos.eliminarDiscoVinilo();
				break;
			case 14:
				tiendaDiscos.asignarArtistaVinilo();
				break;
			case 15:
				System.out.println("Fin del programa.");
				System.exit(0);
				break;
			default:
				System.out.println("Opci�n no v�lida, vuelva a intentarlo.");
			}

		} while (opcion != 15);

		input.close();
	}

}