package clases;

public class Disco {
	String nombre;
	double precio;
	int numCanciones;
	Artista artista;

	public Disco() {
		this.nombre = "";
		this.precio = 0;
		this.numCanciones = 0;

	}

	public Disco(String nombre, double precio, int numCanciones) {
		this.nombre = nombre;
		this.precio = precio;
		this.numCanciones = numCanciones;

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getNumCanciones() {
		return numCanciones;
	}

	public void setNumCanciones(int numCanciones) {
		this.numCanciones = numCanciones;
	}

	public Artista getArtista() {
		return artista;
	}

	public void setArtista(Artista artista) {
		this.artista = artista;
	}

	@Override
	public String toString() {
		return "Disco [nombre=" + nombre + ", precio=" + precio + ", numCanciones=" + numCanciones + ", artista="
				+ artista + "]";
	}

	

}
