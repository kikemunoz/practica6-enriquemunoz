package clases;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

/**
 * 
 * @autor kike
 * 
 */
public class GestorDiscos {
	/**
	 * Declaracion e inicializacion de la clase Scanner
	 */
	static Scanner input = new Scanner(System.in);
	/**
	 * Declaracion de Arraylist
	 */
	private ArrayList<Artista> listaArtistas;
	private ArrayList<DiscoCd> listaDiscosCd;
	private ArrayList<DiscoVinilo> listaDiscosVinilo;

	/**
	 * Constructor que inicializa Arraylists de Artista y de Disco
	 */
	public GestorDiscos() {
		listaArtistas = new ArrayList<Artista>();
		listaDiscosCd = new ArrayList<DiscoCd>();
		listaDiscosVinilo = new ArrayList<DiscoVinilo>();
	}

	/**
	 * Metodo que comprueba si existe alg�n artista
	 * 
	 * @param nombre del artista
	 * 
	 * @return valor verdadero o falso
	 */
	public boolean existeArtista(String nombre) {
		for (Artista artista : listaArtistas) {
			if (artista != null && artista.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Metodo que comprueba si existe alg�n CD por nombre.
	 * 
	 * @param nombre del CD
	 * 
	 * @return valor verdadero o falso
	 */
	public boolean existeCd(String nombre) {
		for (DiscoCd disco : listaDiscosCd) {
			if (disco != null && disco.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo que comprueba si existe alg�n Vinilo por nombre.
	 * 
	 * @param nombre del Vinilo
	 * 
	 * @return valor verdadero o falso
	 */
	public boolean existeVinilo(String nombre) {
		for (DiscoVinilo disco : listaDiscosVinilo) {
			if (disco != null && disco.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para dar de alta un artista, si el nombre del artista ya existe, no lo
	 * creara.
	 * 
	 */
	public void altaArtista() {
		boolean error = false;
		System.out.println("Introduce el nombre:");
		String nombre = input.nextLine();
		if (!existeArtista(nombre)) {
			Artista nuevoArtista = new Artista();
			nuevoArtista.setNombre(nombre);
			System.out.println("Introduce el pa�s:");
			String pais = input.nextLine();
			nuevoArtista.setPais(pais);
			do {
				try {
					System.out.println("Introduce la edad:");
					int edad = input.nextInt();
					nuevoArtista.setEdad(edad);
					error = false;
				} catch (InputMismatchException e) {
					System.out.println("Debe introducir n�meros");
					error = true;
					input.nextLine();
				}

			} while (error);

			input.nextLine();
			listaArtistas.add(nuevoArtista);
			System.out.println("\n");
			System.out.println("Artista dado de alta.");
		} else {
			System.out.println("El artista ya existe.");
		}
	}

	/**
	 * Metodo para listar artistas
	 * 
	 */
	public void listarArtistas() {
		if (listaArtistas.size() > 0) {
			for (Artista artista : listaArtistas) {
				if (artista != null) {
					System.out.println(artista);
				}
			}
		} else {
			System.out.println("No hay ning�n artista dado de alta.");
		}
	}

	/**
	 * Metodo para buscar un artista, si el no existe ningun artista por el nombre,
	 * devuelve null.
	 * 
	 * @return Objeto Artista
	 */
	public Artista buscarArtista() {
		System.out.println("Introduce el nombre del artista:");
		String nombreArtista = input.nextLine().toLowerCase();
		for (Artista artista : listaArtistas) {
			if (artista != null && artista.getNombre().equals(nombreArtista)) {
				return artista;
			}
		}
		System.out.println("No existe ning�n artista por ese nombre.");
		return null;
	}

	/**
	 * Metodo para eliminar un artista por el nombre si existe.
	 * 
	 */
	public void eliminarArtista() {
		System.out.println("Introduce en nombre del artista a eliminar:");
		String nombreArtista = input.nextLine().toLowerCase();
		if (existeArtista(nombreArtista)) {
			Iterator<Artista> iteradorArtistas = listaArtistas.iterator();
			while (iteradorArtistas.hasNext()) {
				Artista artista = iteradorArtistas.next();
				if (artista.getNombre().equals(nombreArtista)) {
					iteradorArtistas.remove();
				}
			}
			System.out.println("Artista eliminado.");
		} else {
			System.out.println("El nombre del artista no existe.");
		}
	}

	/**
	 * Metodo que comprueba si existe alg�n CD
	 * 
	 * @param nombre del CD
	 * 
	 * @return valor verdadero o falso
	 */
	public boolean existeDiscoCd(String nombre) {
		for (Disco disco : listaDiscosCd) {
			if (disco != null && disco.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo que comprueba si existe alg�n Vinilo
	 * 
	 * @param nombre del vinilo
	 * 
	 * @return valor verdadero o falso
	 */
	public boolean existeDiscoVinilo(String nombre) {
		for (Disco disco : listaDiscosVinilo) {
			if (disco != null && disco.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para dar de alta un CD, si el nombre del CD ya existe, no lo creara.
	 * 
	 */
	public void altaDiscoCd() {
		boolean error = false;
		System.out.println("Introduce el nombre:");
		String nombre = input.nextLine();
		if (!existeDiscoCd(nombre)) {
			DiscoCd nuevoDiscoCd = new DiscoCd();
			nuevoDiscoCd.setNombre(nombre);
			do {
				try {
					System.out.println("Introduce el precio:");
					int precio = input.nextInt();
					nuevoDiscoCd.setPrecio(precio);
					error = false;
				} catch (InputMismatchException e) {
					System.out.println("Debe introducir n�meros");
					error = true;
					input.nextLine();
				}
			} while (error);
			input.nextLine();

			do {
				try {
					System.out.println("Introduce el n�mero de canciones:");
					int numCancion = input.nextInt();
					nuevoDiscoCd.setNumCanciones(numCancion);
					error = false;
				} catch (InputMismatchException e) {
					System.out.println("Debe introducir n�meros");
					error = true;
					input.nextLine();
				}

			} while (error);

			do {
				try {
					System.out.println("Introduce n�mero de CDs:");
					int numCds = input.nextInt();
					nuevoDiscoCd.setNumCds(numCds);
					error = false;
				} catch (InputMismatchException e) {
					System.out.println("Debe introducir n�meros");
					error = true;
					input.nextLine();
				}
			} while (error);
			input.nextLine();
			System.out.println("Introduce el tipo de CD:");
			String tipoCd = input.nextLine();
			nuevoDiscoCd.setTipo(tipoCd);
			listaDiscosCd.add(nuevoDiscoCd);
			System.out.println("\n");
		} else {
			System.out.println("El disco ya existe.");
		}
	}

	/**
	 * Metodo para dar de alta un Vinilo, si el nombre del vinilo ya existe, no lo
	 * creara.
	 * 
	 */
	public void altaDiscoVinilo() {
		boolean error = false;
		System.out.println("Introduce el nombre:");
		String nombre = input.nextLine();
		if (!existeDiscoVinilo(nombre)) {
			DiscoVinilo nuevoDiscoVinilo = new DiscoVinilo();
			nuevoDiscoVinilo.setNombre(nombre);
			do {
				try {
					System.out.println("Introduce el precio:");
					int precio = input.nextInt();
					nuevoDiscoVinilo.setPrecio(precio);
					error = false;
				} catch (InputMismatchException e) {
					System.out.println("Debe introducir n�meros");
					error = true;
					input.nextLine();
				}
			} while (error);
			do {
				try {
					System.out.println("Introduce el n�mero de canciones:");
					int numCancion = input.nextInt();
					nuevoDiscoVinilo.setNumCanciones(numCancion);
					error = false;
				} catch (InputMismatchException e) {
					System.out.println("Debe introducir n�meros");
					error = true;
					input.nextLine();
				}
			} while (error);
			do {
				try {
			System.out.println("Introduce el formato (33/45/78):");
			int formato = input.nextInt();
			nuevoDiscoVinilo.setFormato(formato);
			error = false;
				} catch (InputMismatchException e) {
					System.out.println("Debe introducir n�meros");
					error = true;
					input.nextLine();
				}
			} while (error);
			input.nextLine();
			System.out.println("Introduce el color:");
			String color = input.nextLine();
			nuevoDiscoVinilo.setColor(color);
			System.out.println("Introduce el material:");
			String material = input.nextLine();
			nuevoDiscoVinilo.setMaterial(material);
			listaDiscosVinilo.add(nuevoDiscoVinilo);
			System.out.println("\n");
		} else {
			System.out.println("El disco ya existe.");
		}
	}

	/**
	 * Metodo para listar CDs
	 */
	public void listarDiscosCd() {
		for (Disco disco : listaDiscosCd) {
			if (disco != null) {
				System.out.println(disco);
			}
		}
	}

	/**
	 * Metodo para listar Vinilos
	 */
	public void listarDiscosVinilo() {
		for (Disco disco : listaDiscosVinilo) {
			if (disco != null) {
				System.out.println(disco);
			}
		}
	}

	/**
	 * Metodo para buscar un CD, si no existe ningun CD por el nombre, devuelve
	 * null.
	 * 
	 * @return Objeto DiscoCd
	 */
	public Disco buscarDiscoCd() {
		System.out.println("Introduce el nombre del CD:");
		String nombreCd = input.nextLine().toLowerCase();
		for (DiscoCd disco : listaDiscosCd) {
			if (disco != null && disco.getNombre().equals(nombreCd)) {
				return disco;
			}
		}
		System.out.println("No existe ning�n CD por ese nombre.");
		return null;
	}

	/**
	 * Metodo para buscar un Vinilo, si no existe ningun Vinilo por el nombre,
	 * devuelve null.
	 * 
	 * @return Objeto DiscoVinilo
	 */
	public Disco buscarDiscoVinilo() {
		System.out.println("Introduce el nombre del Vinilo:");
		String nombreVinilo = input.nextLine().toLowerCase();
		for (DiscoVinilo disco : listaDiscosVinilo) {
			if (disco != null && disco.getNombre().equals(nombreVinilo)) {
				return disco;
			}
		}
		System.out.println("No existe ning�n Vinilo por ese nombre.");
		return null;
	}

	/**
	 * Metodo para eliminar un CD por el nombre si existe.
	 * 
	 */
	public void eliminarDiscoCd() {
		System.out.println("Introduce en nombre del CD a eliminar:");
		String nombreCd = input.nextLine().toLowerCase();
		if (existeCd(nombreCd)) {
			Iterator<DiscoCd> iteradorCd = listaDiscosCd.iterator();
			while (iteradorCd.hasNext()) {
				DiscoCd disco = iteradorCd.next();
				if (disco.getNombre().equals(nombreCd)) {
					iteradorCd.remove();
				}
			}
			System.out.println("CD eliminado.");
		} else {
			System.out.println("El nombre del CD no existe.");
		}
	}

	/**
	 * Metodo para eliminar un Vinilo por el nombre si existe.
	 * 
	 */
	public void eliminarDiscoVinilo() {
		System.out.println("Introduce en nombre del Vinilo a eliminar:");
		String nombreVinilo = input.nextLine().toLowerCase();
		if (existeCd(nombreVinilo)) {
			Iterator<DiscoVinilo> iteradorVinilo = listaDiscosVinilo.iterator();
			while (iteradorVinilo.hasNext()) {
				DiscoVinilo disco = iteradorVinilo.next();
				if (disco.getNombre().equals(nombreVinilo)) {
					iteradorVinilo.remove();
				}
			}
			System.out.println("Vinilo eliminado.");
		} else {
			System.out.println("El nombre del Vinilo no existe.");
		}
	}

	/**
	 * Metodo para asignar un artista a un CD
	 * 
	 */
	public void asignarArtistaCd() {
		Artista artista = buscarArtista();
		if (artista != null) {
			Disco disco = buscarDiscoCd();
			if (disco != null) {
				disco.setArtista(artista);
				System.out.println("");
				System.out.println("Artista asignado");
			}
		}
	}

	/**
	 * Metodo para asignar un artista a un Vinilo
	 * 
	 */
	public void asignarArtistaVinilo() {
		Artista artista = buscarArtista();
		if (artista != null) {
			Disco disco = buscarDiscoVinilo();
			if (disco != null) {
				disco.setArtista(artista);
				System.out.println("");
				System.out.println("Artista asignado");
			}
		}
	}

}
