package clases;

public class DiscoCd extends Disco {

	int numCds;
	String tipo;
	int anno;
	
	public DiscoCd() {
		super();
		this.numCds = 0;
		this.tipo = "";
		this.anno = 0;
	}

	
	public DiscoCd(String nombre, double precio, int numCanciones, int numCds, String tipo, int anno) {
		super(nombre, precio, numCanciones);
		this.numCds = numCds;
		this.tipo = tipo;
		this.anno = anno;
	}

	public int getNumCds() {
		return numCds;
	}

	public void setNumCds(int numCds) {
		this.numCds = numCds;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	
	@Override
	public String toString() {
		return "DiscoCd [numCds=" + numCds + ", tipo=" + tipo + ", anno=" + anno + ", nombre=" + nombre + ", precio="
				+ precio + ", numCanciones=" + numCanciones + ", artista=" + artista + "]";
	}

	
}
