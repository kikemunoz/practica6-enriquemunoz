package clases;

public class DiscoVinilo extends Disco {

	int formato;
	String color;
	String material;

	public DiscoVinilo() {
		super();
		this.formato = 0;
		this.color = "";
		this.material = "";
	}

	public DiscoVinilo(String nombre, double precio, int numCanciones, int formato, String color, String material) {
		super(nombre, precio, numCanciones);
		this.formato = formato;
		this.color = color;
		this.material = material;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getFormato() {
		return formato;
	}

	public void setFormato(int formato) {
		this.formato = formato;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	@Override
	public String toString() {
		return "DiscoVinilo [formato=" + formato + ", color=" + color + ", material=" + material + ", nombre=" + nombre
				+ ", precio=" + precio + ", numCanciones=" + numCanciones + ", artista=" + artista + "]";
	}

}
